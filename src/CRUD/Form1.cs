using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CRUD
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        SqlConnection sqlCon = null;
        private string strcon = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=ClientesDaLoja;Data Source=DESKTOP-7QJT193\SQLEXPRESS";
        private string strSql = String.Empty;

        public SqlConnection SqlCon { get => sqlCon; set => sqlCon = value; }

        private void tsbSalvar_Click(object sender, EventArgs e)
        {
            strSql = "insert into ClientesDaLoja (Id, Nome, CPF) values (@Id, @Nome, @CPF)";
            SqlCon = new SqlConnection(strcon);
            SqlCommand comando = new SqlCommand(strSql, SqlCon);

            comando.Parameters.Add("Id", SqlDbType.Int).Value = txtId.Text;
            comando.Parameters.Add("Nome", SqlDbType.VarChar).Value = txtNome.Text;
            comando.Parameters.Add("CPF", SqlDbType.VarChar).Value = mskCPF.Text;

            try
            {
                SqlCon.Open();
                comando.ExecuteNonQuery();
                MessageBox.Show("Cadastro Realizado com sucesso!");

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }

            finally
            {
                SqlCon.Close();
            }



        }

        private void tsbBuscar_Click(object sender, EventArgs e)
        {
            strSql = "select * from ClientesDaLoja where Id=@Id";
            SqlCon = new SqlConnection(strcon);
            SqlCommand comando = new SqlCommand(strSql, SqlCon);

            comando.Parameters.Add("@Id", SqlDbType.Int).Value = tstIdBuscar.Text;


            try
            {
                if (tstIdBuscar.Text == String.Empty)
                {
                    throw new Exception("Voc� precisa digitar um ID");
                }
                SqlCon.Open();

                SqlDataReader dr = comando.ExecuteReader();
                if (dr.HasRows == false)
                {
                    throw new Exception("ID n�o cadastrado");
                }

                while (dr.Read())
                {
                    txtId.Text = Convert.ToString(dr.GetInt32("Id"));
                    txtNome.Text = Convert.ToString(dr.GetString("Nome"));
                    mskCPF.Text = Convert.ToString(dr.GetString("CPF"));
                };



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            finally
            {
                SqlCon.Close();

            }



        }

        private void tsbEditar_Click(object sender, EventArgs e)
        {
            strSql = "update ClientesDaLoja set Id=@Id, Nome=@Nome, CPF=@CPF where Id=@IdBuscar";
            sqlCon = new SqlConnection(strcon);
            SqlCommand comando = new SqlCommand(strSql, SqlCon);

            comando.Parameters.Add("@IdBuscar", SqlDbType.Int).Value = tstIdBuscar.Text;

            comando.Parameters.Add("Id", SqlDbType.Int).Value = txtId.Text;
            comando.Parameters.Add("Nome", SqlDbType.VarChar).Value = txtNome.Text;
            comando.Parameters.Add("CPF", SqlDbType.VarChar).Value = mskCPF.Text;

            try
            {
                SqlCon.Open();
                comando.ExecuteNonQuery();
                MessageBox.Show("Cadastro atualizado com sucesso");

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                SqlCon.Close();
            }
        }

        private void tsbDeletar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja excluir o cliente?", "Cuidado", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
            {
                MessageBox.Show("Opera��o cancelada");
            }

            else
            {
                strSql = "delete from ClientesDaLoja where Id=@Id";
                SqlCon = new SqlConnection(strcon);
                SqlCommand comando = new SqlCommand(strSql, SqlCon);

                comando.Parameters.Add("@Id", SqlDbType.Int).Value = tstIdBuscar.Text;

                try
                {
                    SqlCon.Open();
                    comando.ExecuteNonQuery();
                    MessageBox.Show("Funcionario deletado.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    SqlCon.Close();
                }
            }
        }

        private void tsbNovo_Click(object sender, EventArgs e)
        {
            tsbNovo.Enabled = false;
            tsbSalvar.Enabled = true;
            tsbEditar.Enabled = false;
            tsbDeletar.Enabled = false;
            tstIdBuscar.Enabled = false;
            tsbCancelar.Enabled = true;

            txtId.Enabled = true;
            txtNome.Enabled = true;
            mskCPF.Enabled = true;

        }
        private void tsbDeletar_Click(object sender, EventArgs e)
        {
            

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            tsbNovo.Enabled = true;
            tsbSalvar.Enabled = false;
            tsbEditar.Enabled = false;
            tsbDeletar.Enabled = false;
            tstIdBuscar.Enabled = true;
            tsbBuscar.Enabled = true;
            txtId.Enabled = true;
            txtNome.Enabled = true;
            mskCPF.Enabled = true;

            txtId.Text = "";
            tstIdBuscar.Text = "";
            txtNome.Text = "";
            mskCPF.Text = "";
        }
    }
}